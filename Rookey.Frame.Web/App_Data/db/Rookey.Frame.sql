USE [master]
GO
/****** Object:  Database [Rookey_Frame]    Script Date: 06/22/2017 11:31:47 ******/
CREATE DATABASE [Rookey_Frame] ON  PRIMARY 
( NAME = N'Rookey_Frame', FILENAME = N'D:\db\Rookey_Frame.mdf' , SIZE = 4352KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'HkTempLog', FILENAME = N'D:\db\Rookey_Frame.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 1024KB )
GO
ALTER DATABASE [Rookey_Frame] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Rookey_Frame].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Rookey_Frame] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Rookey_Frame] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Rookey_Frame] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Rookey_Frame] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Rookey_Frame] SET ARITHABORT OFF
GO
ALTER DATABASE [Rookey_Frame] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [Rookey_Frame] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Rookey_Frame] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Rookey_Frame] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Rookey_Frame] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Rookey_Frame] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Rookey_Frame] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Rookey_Frame] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Rookey_Frame] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Rookey_Frame] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Rookey_Frame] SET  DISABLE_BROKER
GO
ALTER DATABASE [Rookey_Frame] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Rookey_Frame] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Rookey_Frame] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Rookey_Frame] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Rookey_Frame] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Rookey_Frame] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Rookey_Frame] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Rookey_Frame] SET  READ_WRITE
GO
ALTER DATABASE [Rookey_Frame] SET RECOVERY FULL
GO
ALTER DATABASE [Rookey_Frame] SET  MULTI_USER
GO
ALTER DATABASE [Rookey_Frame] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Rookey_Frame] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'Rookey_Frame', N'ON'
GO
USE [Rookey_Frame]
GO
/****** Object:  Table [dbo].[Desktop_Permission]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Desktop_Permission](
	[SetType] [int] NOT NULL,
	[TypeId] [uniqueidentifier] NULL,
	[Desktop_ItemId] [uniqueidentifier] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Desktop_ItemTab]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Desktop_ItemTab](
	[Desktop_ItemId] [uniqueidentifier] NULL,
	[Title] [varchar](100) NULL,
	[Url] [varchar](500) NULL,
	[MoreUrl] [varchar](500) NULL,
	[Sort] [int] NOT NULL,
	[Des] [varchar](500) NULL,
	[Flag] [varchar](20) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Desktop_Item]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Desktop_Item](
	[Name] [varchar](100) NULL,
	[Sort] [int] NOT NULL,
	[IsCanClose] [bit] NOT NULL,
	[Des] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Desktop_GridField]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Desktop_GridField](
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[FieidName] [varchar](500) NULL,
	[Width] [int] NOT NULL,
	[Sort] [int] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_WorkToDoListHistory]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_WorkToDoListHistory](
	[Code] [varchar](200) NULL,
	[Title] [varchar](500) NULL,
	[Bpm_WorkFlowInstanceId] [uniqueidentifier] NULL,
	[Bpm_WorkNodeInstanceId] [uniqueidentifier] NULL,
	[OrgM_EmpId] [uniqueidentifier] NULL,
	[Launcher] [varchar](50) NULL,
	[LaunchTime] [datetime] NOT NULL,
	[StartDate] [datetime] NULL,
	[FinishDate] [datetime] NULL,
	[Status] [int] NOT NULL,
	[ApprovalOpinions] [varchar](2000) NULL,
	[WorkAction] [int] NOT NULL,
	[Bpm_WorkNodeId] [uniqueidentifier] NULL,
	[NextNodeHandler] [varchar](2000) NULL,
	[ModuleId] [uniqueidentifier] NOT NULL,
	[RecordId] [uniqueidentifier] NOT NULL,
	[IsInitHandler] [bit] NOT NULL,
	[ParentId] [uniqueidentifier] NULL,
	[IsParentTodo] [bit] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] NOT NULL,
	[FlowStatus] [int] NULL,
	[PrincipalId] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_WorkToDoList]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_WorkToDoList](
	[Code] [varchar](200) NULL,
	[Title] [varchar](500) NULL,
	[Bpm_WorkFlowInstanceId] [uniqueidentifier] NULL,
	[Bpm_WorkNodeInstanceId] [uniqueidentifier] NULL,
	[OrgM_EmpId] [uniqueidentifier] NULL,
	[Launcher] [varchar](50) NULL,
	[LaunchTime] [datetime] NOT NULL,
	[StartDate] [datetime] NULL,
	[FinishDate] [datetime] NULL,
	[Status] [int] NOT NULL,
	[ApprovalOpinions] [varchar](2000) NULL,
	[WorkAction] [int] NOT NULL,
	[Bpm_WorkNodeId] [uniqueidentifier] NULL,
	[NextNodeHandler] [varchar](2000) NULL,
	[ModuleId] [uniqueidentifier] NOT NULL,
	[RecordId] [uniqueidentifier] NOT NULL,
	[IsInitHandler] [bit] NOT NULL,
	[ParentId] [uniqueidentifier] NULL,
	[IsParentTodo] [bit] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
	[PrincipalId] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_WorkNodeInstanceHistory]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_WorkNodeInstanceHistory](
	[SerialNo] [varchar](100) NULL,
	[Bpm_WorkFlowInstanceId] [uniqueidentifier] NULL,
	[Bpm_WorkNodeId] [uniqueidentifier] NULL,
	[StartDate] [datetime] NULL,
	[FinishDate] [datetime] NULL,
	[Status] [int] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_WorkNodeInstance]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_WorkNodeInstance](
	[SerialNo] [varchar](100) NULL,
	[Bpm_WorkFlowInstanceId] [uniqueidentifier] NULL,
	[Bpm_WorkNodeId] [uniqueidentifier] NULL,
	[StartDate] [datetime] NULL,
	[FinishDate] [datetime] NULL,
	[Status] [int] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_WorkNode]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_WorkNode](
	[Code] [varchar](200) NULL,
	[Bpm_WorkFlowId] [uniqueidentifier] NULL,
	[Name] [varchar](200) NULL,
	[DisplayName] [varchar](200) NULL,
	[Sort] [int] NOT NULL,
	[Sys_FormId] [uniqueidentifier] NULL,
	[FormUrl] [varchar](500) NULL,
	[HandlerType] [int] NOT NULL,
	[HandleRange] [varchar](4000) NULL,
	[HandleStrategy] [int] NOT NULL,
	[FormFieldName] [varchar](50) NULL,
	[AutoJumpRule] [varchar](50) NULL,
	[BackType] [int] NOT NULL,
	[WorkNodeType] [int] NOT NULL,
	[Bpm_WorkFlowSubId] [uniqueidentifier] NULL,
	[SubFlowType] [int] NOT NULL,
	[Width] [int] NOT NULL,
	[Height] [int] NOT NULL,
	[Top] [int] NOT NULL,
	[Left] [int] NOT NULL,
	[TagId] [varchar](100) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_WorkLine]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_WorkLine](
	[Code] [varchar](200) NULL,
	[Bpm_WorkFlowId] [uniqueidentifier] NULL,
	[Bpm_WorkNodeStartId] [uniqueidentifier] NULL,
	[Bpm_WorkNodeEndId] [uniqueidentifier] NULL,
	[Note] [varchar](200) NULL,
	[IsCustomerCondition] [bit] NOT NULL,
	[FormCondition] [varchar](500) NULL,
	[DutyCondition] [varchar](4000) NULL,
	[DeptCondition] [varchar](4000) NULL,
	[SqlCondition] [varchar](4000) NULL,
	[M] [float] NOT NULL,
	[FromTagId] [varchar](50) NULL,
	[ToTagId] [varchar](50) NULL,
	[TagId] [varchar](50) NULL,
	[LineType] [varchar](10) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_WorkFlowInstanceHistory]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_WorkFlowInstanceHistory](
	[Code] [varchar](200) NULL,
	[Title] [varchar](200) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[Bpm_WorkFlowId] [uniqueidentifier] NULL,
	[Status] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[OrgM_EmpId] [uniqueidentifier] NULL,
	[RecordId] [uniqueidentifier] NOT NULL,
	[EndDate] [datetime] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_WorkFlowInstance]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_WorkFlowInstance](
	[Code] [varchar](200) NULL,
	[Title] [varchar](200) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[Bpm_WorkFlowId] [uniqueidentifier] NULL,
	[Status] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[OrgM_EmpId] [uniqueidentifier] NULL,
	[RecordId] [uniqueidentifier] NOT NULL,
	[EndDate] [datetime] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_WorkFlow]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_WorkFlow](
	[Name] [varchar](200) NULL,
	[DisplayName] [varchar](200) NULL,
	[Bpm_FlowClassId] [uniqueidentifier] NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[ValidStartTime] [datetime] NULL,
	[ValidEndTime] [datetime] NULL,
	[OrgM_EmpId] [uniqueidentifier] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_SubWorkFlow]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_SubWorkFlow](
	[Bpm_WorkFlowId] [uniqueidentifier] NULL,
	[Bpm_WorkNodeId] [uniqueidentifier] NULL,
	[Bpm_WorkFlowChildId] [uniqueidentifier] NULL,
	[Memo] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_NodeBtnConfig]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_NodeBtnConfig](
	[Code] [varchar](100) NULL,
	[Bpm_WorkFlowId] [uniqueidentifier] NULL,
	[Bpm_WorkNodeId] [uniqueidentifier] NULL,
	[Bpm_FlowBtnId] [uniqueidentifier] NULL,
	[BtnDisplay] [varchar](100) NULL,
	[IsEnabled] [bit] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_FlowProxy]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_FlowProxy](
	[Bpm_WorkFlowId] [uniqueidentifier] NULL,
	[Bpm_WorkNodeId] [uniqueidentifier] NULL,
	[OrgM_EmpId] [uniqueidentifier] NULL,
	[OrgM_EmpProxyId] [uniqueidentifier] NULL,
	[IsValid] [bit] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_FlowClass]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_FlowClass](
	[Name] [varchar](200) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[Des] [varchar](2000) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bpm_FlowBtn]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bpm_FlowBtn](
	[ButtonText] [varchar](100) NULL,
	[ButtonIcon] [varchar](100) NULL,
	[ClickMethod] [varchar](100) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[ButtonType] [int] NOT NULL,
	[Sort] [int] NOT NULL,
	[Memo] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_UserRole]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_UserRole](
	[Sys_UserId] [uniqueidentifier] NULL,
	[Sys_RoleId] [uniqueidentifier] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_UserQuckMenu]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_UserQuckMenu](
	[Sys_UserId] [uniqueidentifier] NULL,
	[Sys_MenuId] [uniqueidentifier] NULL,
	[Sort] [int] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_UserPermissionFun]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_UserPermissionFun](
	[Sys_UserId] [uniqueidentifier] NULL,
	[FunId] [uniqueidentifier] NOT NULL,
	[FunType] [int] NOT NULL,
	[Des] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_UserPermissionField]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_UserPermissionField](
	[Sys_UserId] [uniqueidentifier] NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[CanViewFields] [varchar](500) NULL,
	[CanAddFields] [varchar](500) NULL,
	[CanEditFields] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_UserPermissionData]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_UserPermissionData](
	[Sys_UserId] [uniqueidentifier] NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[CanViewOrgIds] [varchar](500) NULL,
	[CanEditOrgIds] [varchar](500) NULL,
	[CanDelOrgIds] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_UserGrid]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_UserGrid](
	[Sys_UserId] [uniqueidentifier] NULL,
	[Sys_GridId] [uniqueidentifier] NULL,
	[IsDefault] [bit] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_User]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_User](
	[UserName] [varchar](30) NULL,
	[AliasName] [varchar](30) NULL,
	[Sys_OrganizationId] [uniqueidentifier] NULL,
	[PasswordHash] [varchar](100) NULL,
	[PasswordSalt] [varchar](100) NULL,
	[PwdProtectQuest] [varchar](200) NULL,
	[PwdProtectAnswer] [varchar](200) NULL,
	[IsActivated] [bit] NOT NULL,
	[ActivatedDate] [datetime] NULL,
	[IsValid] [bit] NOT NULL,
	[ExpiryDate] [datetime] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_TempUser]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_TempUser](
	[FieldInfo1] [varchar](500) NULL,
	[FieldInfo2] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_SystemSet]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_SystemSet](
	[Name] [varchar](100) NULL,
	[Value] [varchar](4000) NULL,
	[SetType] [int] NOT NULL,
	[Sys_UserId] [uniqueidentifier] NULL,
	[Des] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_RoleForm]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_RoleForm](
	[Sys_RoleId] [uniqueidentifier] NULL,
	[Sys_FormId] [uniqueidentifier] NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_Role]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Role](
	[Name] [varchar](100) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[OtherParentRoles] [varchar](1000) NULL,
	[IsValid] [bit] NOT NULL,
	[ExpiryDate] [datetime] NULL,
	[Des] [varchar](300) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_PermissionFun]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_PermissionFun](
	[Sys_RoleId] [uniqueidentifier] NULL,
	[FunId] [uniqueidentifier] NOT NULL,
	[FunType] [int] NOT NULL,
	[Des] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_PermissionField]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_PermissionField](
	[Sys_RoleId] [uniqueidentifier] NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[CanViewFields] [varchar](500) NULL,
	[CanAddFields] [varchar](500) NULL,
	[CanEditFields] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_PermissionData]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_PermissionData](
	[Sys_RoleId] [uniqueidentifier] NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[CanViewOrgIds] [varchar](500) NULL,
	[CanEditOrgIds] [varchar](500) NULL,
	[CanDelOrgIds] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_PageCache]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_PageCache](
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[CacheKey] [varchar](500) NULL,
	[CachePageType] [int] NOT NULL,
	[PageHtml] [varchar](max) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_Organization]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Organization](
	[Name] [varchar](100) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[Flag] [varchar](50) NULL,
	[Des] [varchar](200) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_Module]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Module](
	[Name] [varchar](100) NULL,
	[Display] [varchar](100) NULL,
	[TableName] [varchar](100) NULL,
	[DataSourceType] [int] NOT NULL,
	[PrimaryKeyFields] [varchar](100) NULL,
	[TitleKey] [varchar](100) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[ModuleLogo] [varchar](100) NULL,
	[Sort] [int] NOT NULL,
	[IsAllowAdd] [bit] NOT NULL,
	[IsAllowEdit] [bit] NOT NULL,
	[IsAllowDelete] [bit] NOT NULL,
	[IsAllowCopy] [bit] NOT NULL,
	[IsAllowImport] [bit] NOT NULL,
	[IsAllowExport] [bit] NOT NULL,
	[IsEnableAttachment] [bit] NOT NULL,
	[IsEnabledRecycle] [bit] NOT NULL,
	[IsEnabledBatchEdit] [bit] NOT NULL,
	[IsEnabledPrint] [bit] NOT NULL,
	[IsEnabledDraft] [bit] NOT NULL,
	[IsMutiSelect] [bit] NOT NULL,
	[IsEnableCodeRule] [bit] NOT NULL,
	[IsCustomerModule] [bit] NOT NULL,
	[StandardJsFolder] [varchar](100) NULL,
	[DetailTopDisplay] [bit] NOT NULL,
	[DetailInGrid] [bit] NOT NULL,
	[AttachModuleTopDisplay] [bit] NOT NULL,
	[FormAttachDisplayStyle] [int] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_Menu]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Menu](
	[Name] [varchar](100) NULL,
	[Display] [varchar](100) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[Sort] [int] NOT NULL,
	[IsValid] [bit] NOT NULL,
	[IsLeaf] [bit] NOT NULL,
	[Icon] [varchar](100) NULL,
	[Url] [varchar](500) NULL,
	[IsNewWinOpen] [bit] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_IconManage]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_IconManage](
	[StyleClassName] [varchar](500) NULL,
	[IconAddr] [varchar](500) NULL,
	[IconClass] [int] NOT NULL,
	[IconType] [int] NOT NULL,
	[Memo] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_GridField]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_GridField](
	[Sys_GridId] [uniqueidentifier] NULL,
	[Sys_FieldId] [uniqueidentifier] NULL,
	[Display] [varchar](100) NULL,
	[MinWidth] [int] NULL,
	[Width] [int] NULL,
	[IsFrozen] [bit] NOT NULL,
	[IsGroupField] [bit] NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[Sort] [int] NOT NULL,
	[IsAllowSearch] [bit] NOT NULL,
	[IsAllowSort] [bit] NOT NULL,
	[IsAllowHide] [bit] NOT NULL,
	[Align] [int] NOT NULL,
	[IsAllowExport] [bit] NOT NULL,
	[FieldFormatter] [varchar](4000) NULL,
	[EditorFormatter] [varchar](4000) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_GridButton]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_GridButton](
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[ButtonText] [varchar](100) NULL,
	[ButtonTagId] [varchar](100) NULL,
	[ButtonIcon] [varchar](100) NULL,
	[ClickMethod] [varchar](100) NULL,
	[IsSystem] [bit] NULL,
	[OperateButtonType] [int] NOT NULL,
	[ParentId] [uniqueidentifier] NULL,
	[GridButtonLocation] [int] NOT NULL,
	[Sort] [int] NOT NULL,
	[IsValid] [bit] NOT NULL,
	[AfterSeparator] [bit] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_Grid]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Grid](
	[Name] [varchar](100) NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[GridType] [int] NOT NULL,
	[MaxSearchNum] [int] NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[AddFilterRow] [bit] NULL,
	[ShowCheckBox] [bit] NULL,
	[MutiSelect] [bit] NULL,
	[ButtonLocation] [int] NULL,
	[TreeField] [varchar](100) NULL,
	[IsTreeGrid] [bit] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_FormField]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_FormField](
	[Sys_FormId] [uniqueidentifier] NULL,
	[Sys_FieldId] [uniqueidentifier] NULL,
	[Display] [varchar](100) NULL,
	[IsAllowAdd] [bit] NULL,
	[IsAllowEdit] [bit] NULL,
	[IsAllowBatchEdit] [bit] NULL,
	[IsAllowCopy] [bit] NULL,
	[IsRequired] [bit] NULL,
	[IsUnique] [bit] NULL,
	[MinCharLen] [int] NULL,
	[MaxCharLen] [int] NULL,
	[MinValue] [decimal](38, 6) NULL,
	[MaxValue] [decimal](38, 6) NULL,
	[GroupName] [varchar](100) NULL,
	[GroupIcon] [varchar](500) NULL,
	[TabName] [varchar](100) NULL,
	[TabIcon] [varchar](500) NULL,
	[ControlType] [int] NOT NULL,
	[ValidateType] [int] NOT NULL,
	[Width] [int] NULL,
	[DefaultValue] [varchar](100) NULL,
	[NullTipText] [varchar](100) NULL,
	[ValueField] [varchar](100) NULL,
	[TextField] [varchar](100) NULL,
	[UrlOrData] [varchar](500) NULL,
	[FilterCondition] [varchar](500) NULL,
	[IsMultiSelect] [bit] NULL,
	[RowNo] [int] NOT NULL,
	[ColNo] [int] NOT NULL,
	[BeforeIcon] [varchar](500) NULL,
	[RowEditRowNo] [int] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_Form]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Form](
	[Name] [varchar](100) NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[Width] [int] NULL,
	[Height] [int] NULL,
	[ModuleEditMode] [int] NULL,
	[InputWidth] [int] NOT NULL,
	[LabelWidth] [int] NOT NULL,
	[SpaceWidth] [int] NOT NULL,
	[RightToken] [varchar](10) NULL,
	[LabelAlign] [int] NOT NULL,
	[InputAlign] [int] NOT NULL,
	[ButtonLocation] [int] NULL,
	[IsDefault] [bit] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_Field]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Field](
	[Name] [varchar](100) NULL,
	[Display] [varchar](100) NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[ForeignModuleName] [varchar](100) NULL,
	[Precision] [int] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_Dictionary]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Dictionary](
	[ClassName] [varchar](200) NULL,
	[Name] [varchar](200) NULL,
	[Value] [varchar](100) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[Sort] [int] NOT NULL,
	[IsValid] [bit] NULL,
	[IsDefault] [bit] NULL,
	[Memo] [varchar](200) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_BindDictionary]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_BindDictionary](
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[FieldName] [varchar](500) NULL,
	[ClassName] [varchar](500) NULL,
	[IsValid] [bit] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_BillCodeRule]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_BillCodeRule](
	[Name] [varchar](100) NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[FieldName] [varchar](100) NULL,
	[Prefix] [varchar](20) NULL,
	[IsEnableDate] [bit] NOT NULL,
	[DateFormat] [int] NOT NULL,
	[SerialNumber] [int] NOT NULL,
	[PlaceHolder] [int] NOT NULL,
	[SNLength] [int] NOT NULL,
	[RuleFormat] [varchar](50) NULL,
	[Memo] [varchar](500) NULL,
	[CurrCode] [varchar](50) NULL,
	[NextCode] [varchar](50) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_AttachModuleBind]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_AttachModuleBind](
	[ModuleName] [varchar](100) NULL,
	[Sys_UserId] [uniqueidentifier] NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[Sort] [int] NOT NULL,
	[AttachModuleInGrid] [bit] NOT NULL,
	[IsValid] [bit] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sys_Attachment]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sys_Attachment](
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[RecordId] [uniqueidentifier] NULL,
	[RecordTitleKeyValue] [varchar](300) NULL,
	[FileName] [varchar](200) NULL,
	[FileType] [varchar](50) NULL,
	[FileDes] [varchar](200) NULL,
	[FileSize] [varchar](50) NULL,
	[FileUrl] [varchar](500) NULL,
	[PdfUrl] [varchar](500) NULL,
	[SwfUrl] [varchar](500) NULL,
	[Thumbnail] [varchar](500) NULL,
	[AttachType] [varchar](50) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrgM_EmpDeptDuty]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrgM_EmpDeptDuty](
	[Code] [varchar](100) NULL,
	[OrgM_EmpId] [uniqueidentifier] NULL,
	[OrgM_DeptId] [uniqueidentifier] NULL,
	[OrgM_DutyId] [uniqueidentifier] NULL,
	[IsMainDuty] [bit] NOT NULL,
	[IsValid] [bit] NOT NULL,
	[EffectiveDate] [datetime] NULL,
	[InValidDate] [datetime] NULL,
	[Memo] [varchar](2000) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrgM_Emp]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrgM_Emp](
	[Code] [varchar](100) NULL,
	[Name] [varchar](100) NULL,
	[EName] [varchar](100) NULL,
	[Gender] [int] NOT NULL,
	[BirthdayDate] [datetime] NULL,
	[Height] [int] NULL,
	[Education] [int] NOT NULL,
	[IsMarriage] [bit] NULL,
	[BloodType] [int] NOT NULL,
	[Mobile] [varchar](500) NULL,
	[OfficePhone] [varchar](500) NULL,
	[Email] [varchar](500) NULL,
	[Fax] [varchar](500) NULL,
	[EmpStatus] [int] NOT NULL,
	[EmployeeType] [int] NOT NULL,
	[StartWorkDate] [datetime] NULL,
	[EntryDate] [datetime] NULL,
	[PositiveDate] [datetime] NULL,
	[StatusChangeDate] [datetime] NULL,
	[Nationality] [varchar](500) NULL,
	[Hometown] [varchar](500) NULL,
	[Political] [varchar](500) NULL,
	[Religion] [varchar](500) NULL,
	[Sort] [int] NOT NULL,
	[F1] [varchar](100) NULL,
	[F2] [varchar](100) NULL,
	[F3] [varchar](100) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrgM_Duty]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrgM_Duty](
	[Code] [varchar](100) NULL,
	[Name] [varchar](100) NULL,
	[DutyLevel] [varchar](100) NULL,
	[DutyType] [varchar](100) NULL,
	[IsValid] [bit] NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[InValidDate] [datetime] NULL,
	[Sort] [int] NOT NULL,
	[Memo] [varchar](2000) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrgM_DeptDuty]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrgM_DeptDuty](
	[Code] [varchar](100) NULL,
	[OrgM_DeptId] [uniqueidentifier] NULL,
	[OrgM_DutyId] [uniqueidentifier] NULL,
	[Name] [varchar](100) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[PositionDuty] [varchar](2000) NULL,
	[Establishment] [int] NOT NULL,
	[IsDeptCharge] [bit] NOT NULL,
	[IsValid] [bit] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrgM_Dept]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrgM_Dept](
	[Code] [varchar](100) NULL,
	[Name] [varchar](100) NULL,
	[Alias] [varchar](100) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[DeptType] [varchar](100) NULL,
	[DeptGrade] [varchar](100) NULL,
	[Fax] [varchar](100) NULL,
	[InnerPhone] [varchar](100) NULL,
	[OuterPhone] [varchar](100) NULL,
	[DeptDes] [varchar](2000) NULL,
	[DeptDuty] [varchar](2000) NULL,
	[EmpCount] [int] NOT NULL,
	[FixedPersons] [int] NOT NULL,
	[IsValid] [bit] NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[InValidDate] [datetime] NULL,
	[Sort] [int] NOT NULL,
	[IsCompany] [bit] NOT NULL,
	[LevelDepth] [int] NOT NULL,
	[F1] [varchar](200) NULL,
	[F2] [varchar](100) NULL,
	[F3] [varchar](100) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Msg_To]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Msg_To](
	[Msg_EventNotifyId] [uniqueidentifier] NULL,
	[ReceiverType] [int] NOT NULL,
	[ReceiverRange] [varchar](4000) NULL,
	[OtherReceiver] [varchar](2000) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Msg_Template]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Msg_Template](
	[Name] [varchar](100) NULL,
	[TemplateType] [int] NOT NULL,
	[Title] [varchar](500) NULL,
	[Content] [varchar](max) NULL,
	[ValidStartTime] [datetime] NULL,
	[ValidEndTime] [datetime] NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Msg_SendLog]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Msg_SendLog](
	[EventName] [varchar](100) NULL,
	[Sender] [varchar](50) NULL,
	[Tos] [varchar](2000) NULL,
	[Ccs] [varchar](2000) NULL,
	[Bccs] [varchar](2000) NULL,
	[Subject] [varchar](500) NULL,
	[Content] [varchar](max) NULL,
	[IsSuccess] [bit] NOT NULL,
	[ErrMsg] [varchar](2000) NULL,
	[SendFlag] [varchar](200) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Msg_EventNotify]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Msg_EventNotify](
	[Name] [varchar](100) NULL,
	[Sys_ModuleId] [uniqueidentifier] NULL,
	[Bpm_WorkNodeId] [uniqueidentifier] NULL,
	[EventType] [varchar](100) NULL,
	[NotifyType] [int] NOT NULL,
	[Msg_TemplateId] [uniqueidentifier] NULL,
	[ValidStartTime] [datetime] NULL,
	[ValidEndTime] [datetime] NULL,
	[SendByOper] [bit] NOT NULL,
	[Flag] [varchar](100) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Msg_Cc]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Msg_Cc](
	[Msg_EventNotifyId] [uniqueidentifier] NULL,
	[ReceiverType] [int] NOT NULL,
	[ReceiverRange] [varchar](4000) NULL,
	[OtherReceiver] [varchar](2000) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Msg_Bcc]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Msg_Bcc](
	[Msg_EventNotifyId] [uniqueidentifier] NULL,
	[ReceiverType] [int] NOT NULL,
	[ReceiverRange] [varchar](4000) NULL,
	[OtherReceiver] [varchar](2000) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Monitor_OpExecuteTime]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monitor_OpExecuteTime](
	[ModuleName] [varchar](100) NULL,
	[ControllerName] [varchar](100) NULL,
	[ActionName] [varchar](100) NULL,
	[ExecuteMiniSeconds] [float] NOT NULL,
	[OpUserName] [varchar](50) NULL,
	[ClientIp] [varchar](50) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Log_Operate]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Log_Operate](
	[UserId] [varchar](36) NULL,
	[UserAlias] [varchar](30) NULL,
	[ModuleName] [varchar](100) NULL,
	[OperateType] [varchar](30) NULL,
	[OperateContent] [varchar](max) NULL,
	[OperateTime] [datetime] NOT NULL,
	[ClientIp] [varchar](50) NULL,
	[OperateResult] [varchar](30) NULL,
	[OperateTip] [varchar](2000) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Log_Login]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Log_Login](
	[UserId] [varchar](36) NULL,
	[LoginName] [varchar](30) NULL,
	[LoginTime] [datetime] NOT NULL,
	[LoginIp] [varchar](50) NULL,
	[LoginStatus] [varchar](20) NULL,
	[FailureReason] [varchar](1000) NULL,
	[LoginNum] [int] NOT NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Log_Exception]    Script Date: 06/22/2017 11:31:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Log_Exception](
	[ControllerName] [varchar](100) NULL,
	[ActionName] [varchar](100) NULL,
	[ExceptionName] [varchar](100) NULL,
	[ExceptionMsg] [varchar](max) NULL,
	[ExceptionSource] [varchar](100) NULL,
	[StackTrace] [varchar](2000) NULL,
	[ExceptionTime] [datetime] NOT NULL,
	[ParamsObj] [varchar](500) NULL,
	[Id] [uniqueidentifier] NOT NULL,
	[CreateUserId] [uniqueidentifier] NULL,
	[ModifyUserId] [uniqueidentifier] NULL,
	[CreateUserName] [varchar](30) NULL,
	[ModifyUserName] [varchar](30) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeleteTime] [datetime] NULL,
	[IsDraft] [bit] NOT NULL,
	[OrgId] [uniqueidentifier] NULL,
	[AutoIncrmId] [bigint] IDENTITY(1,1) NOT NULL,
	[FlowStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[GetPageTableByRowNumber]    Script Date: 06/22/2017 11:31:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPageTableByRowNumber]
 @Field nvarchar(1000),
 @TableName  nvarchar(2000),
 @condition  nvarchar(4000),--格式为：and (查询条件)  如'and (key=value and key1=value1)'
 @OrderField nvarchar(100),
 @OrderType int,
 @pageindx int,
 @PageSize  int,
 @RecordCount int output     --记录的总数
as
BEGIN
 --判断是否有排序字段
    if(@OrderField is null or ltrim(rtrim(@OrderField))='')
     begin
       RAISERROR('排序字段不能为空',16,1)
       return
     end
    --组装order语句 
 declare @temp nvarchar(200)
 set @temp=' order by '+@OrderField
 if(@OrderType=1)
  begin 
     set @temp=@temp+' asc '
        end
     else
  begin 
     set @temp=@temp+' desc '
        end
    --组装查询条件，如果没有查询条件，直接跳过
   if(@condition is not null and ltrim(rtrim(@condition))!='')
   begin
     set @condition='where 1=1'+@condition
   end
 else
   begin
     set @condition=''
   end
 --求记录的总数
 declare @Countsql nvarchar(max)
 set @Countsql='select @a= count(1) from '+@TableName +' '+@condition
 exec sp_executesql  @Countsql,N'@a int output',@RecordCount output  
 print @RecordCount
 declare @sql nvarchar(max)
 --分页
 if(@pageindx=1)
  begin
    set @sql=' select top '+cast(@pagesize as nvarchar )+'  '+ @Field+' from '+@TableName +' '+@condition+' '+@temp
  end
 else
  begin
    declare @startNumber   int
    set @startNumber =(@pageindx-1)*@pagesize
    set @sql='select ROW_NUMBER() over('+@temp+') as _number, '+@Field+' from '+@TableName+'  '+@condition 
    set @sql='SET ROWCOUNT '+Convert(varchar(4),@PageSize)+'; WITH SP_TABLE AS('+@sql   +')  SELECT '+@Field+' from SP_TABLE   where  _number>'+CAST(@startNumber as nvarchar) 
  end
 print @sql
 exec(@sql)
END
GO
/****** Object:  StoredProcedure [dbo].[GetBillCode]    Script Date: 06/22/2017 11:31:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBillCode] 
	@ModuleId NVARCHAR(36),
	@returnChar  NVARCHAR(1000)  output  
AS
BEGIN
	DECLARE @TableName nvarchar(255)
	DECLARE @BillNo nvarchar(255)
	DECLARE @TempBillNo nvarchar(255)
	DECLARE @IdentifyExistNo nvarchar(255)
	DECLARE @IdentifyExistSN nvarchar(255)
	DECLARE @IdentifyTempSN nvarchar(255)
	DECLARE @Date DATETIME
	DECLARE @strSql nvarchar(max)
	DECLARE @Prefix nvarchar(255)
	DECLARE @FieldName nvarchar(255)
	DECLARE @IsEnableDate BIT
	DECLARE @DateFormat INT
	DECLARE @SerialNumber INT
	DECLARE @PlaceHolder INT
	DECLARE @SNLength INT
	DECLARE @RuleFormat nvarchar(255)
	SET @Date=GETDATE()
 SELECT @TableName=TableName FROM Sys_Module WHERE Id=@ModuleId
	SELECT @Prefix=Prefix,@IsEnableDate=IsEnableDate,@DateFormat=DateFormat,@SerialNumber=SerialNumber,@PlaceHolder=PlaceHolder,@SNLength=SNLength,@RuleFormat=RuleFormat,@FieldName=FieldName 
 FROM Sys_BillCodeRule WHERE Sys_ModuleId= @ModuleId 
	SET @BillNo=N''
	SET @BillNo=@BillNo+@Prefix
	IF @IsEnableDate=1		
	BEGIN	
		IF @DateFormat=0 SET @BillNo=@BillNo+CONVERT(CHAR(2),@Date,12)
		ELSE IF @DateFormat=1 SET @BillNo=@BillNo+CONVERT(CHAR(4),@Date,112)
		ELSE IF @DateFormat=2 SET @BillNo=@BillNo+CONVERT(CHAR(4),@Date,12)
		ELSE IF @DateFormat=3 SET @BillNo=@BillNo+CONVERT(CHAR(6),@Date,112)
		ELSE IF @DateFormat=4 SET @BillNo=@BillNo+CONVERT(CHAR(6),@Date,12)
		ELSE IF @DateFormat=5 SET @BillNo=@BillNo+CONVERT(CHAR(8),@Date,112)
		ELSE IF @DateFormat=6 SET @BillNo=@BillNo+CONVERT(CHAR(2),@Date,1)+CONVERT(CHAR(2),@Date,12)
		ELSE IF @DateFormat=7 SET @BillNo=@BillNo+CONVERT(CHAR(2),@Date,1)+CONVERT(CHAR(4),@Date,112)
		ELSE IF @DateFormat=8 SET @BillNo=@BillNo+CONVERT(CHAR(2),@Date,12)+'-'+CONVERT(CHAR(2),@Date,1)
		ELSE IF @DateFormat=9 SET @BillNo=@BillNo+CONVERT(CHAR(7),@Date,120)
		ELSE IF @DateFormat=10 SET @BillNo=@BillNo+REPLACE(CONVERT(CHAR(8),@Date,11),'/','-')
		ELSE IF @DateFormat=11 SET @BillNo=@BillNo+CONVERT(CHAR(10),@Date,120)
		ELSE IF @DateFormat=12 SET @BillNo=@BillNo+CONVERT(CHAR(2),@Date,1)+'-'+CONVERT(CHAR(2),@Date,12)
		ELSE IF @DateFormat=13 SET @BillNo=@BillNo+CONVERT(CHAR(2),@Date,1)+'-'+CONVERT(CHAR(4),@Date,112)
		ELSE IF @DateFormat=14 SET @BillNo=@BillNo+REPLACE(CONVERT(CHAR(8),@Date,1),'/','-')
		ELSE IF @DateFormat=15 SET @BillNo=@BillNo+REPLACE(CONVERT(CHAR(10),@Date,101),'/','-')
		ELSE IF @DateFormat=16 SET @BillNo=@BillNo+CONVERT(CHAR(8),@Date,1)
		ELSE IF @DateFormat=17 SET @BillNo=@BillNo+CONVERT(CHAR(10),@Date,101)
		ELSE IF @DateFormat=18 SET @BillNo=@BillNo+CONVERT(CHAR(8),@Date,11)
		ELSE IF @DateFormat=19 SET @BillNo=@BillNo+CONVERT(CHAR(10),@Date,111)
	END
	SET @TempBillNo=@BillNo
	SET @TempBillNo=@TempBillNo+REPLACE(right(cast(power(10,9) as varchar)+@SerialNumber,@SNLength),'0',@PlaceHolder)
	SET @strSql=N''
	SET @strSql =@strSql+ ' SELECT DISTINCT  @IdentifyExistNo='+@FieldName+',@IdentifyExistSN=SUBSTRING('+@FieldName+',LEN('+@FieldName+')-'+CONVERT(NVARCHAR,(@SNLength-1))+',LEN('+@FieldName+'))'
	SET @strSql =@strSql+ ' FROM  '+@TableName
	SET @strSql =@strSql+ ' WHERE LEN('+@FieldName+')='+CONVERT(NVARCHAR,LEN(@TempBillNo))+' AND '+@FieldName+' LIKE '''+@BillNo+'%'''  
	SET @strSql =@strSql+ ' AND SUBSTRING('+@FieldName+',LEN('+@FieldName+')-'+CONVERT(NVARCHAR,(@SNLength-1))+',LEN('+@FieldName+'))=(SELECT MAX(SUBSTRING('+@FieldName+',LEN('+@FieldName+')-'+CONVERT(NVARCHAR,(@SNLength-1))+',LEN('+@FieldName+'))) FROM '+@TableName+' WHERE LEN('+@FieldName+')='+CONVERT(NVARCHAR,LEN(@TempBillNo))
	SET @strSql =@strSql+ ' AND SUBSTRING('+@FieldName+',0,LEN('+@FieldName+')-'+CONVERT(NVARCHAR,(@SNLength-1))+')='''+@BillNo+''''+')'
	EXEC sys.sp_executesql @strSql,N'@IdentifyExistNo nvarchar(255) output,@IdentifyExistSN nvarchar(255) output',@IdentifyExistNo output,@IdentifyExistSN output
	IF @IdentifyExistNo<>''
	BEGIN
	SET @IdentifyTempSN= CONVERT(NVARCHAR,CONVERT(DECIMAL,@IdentifyExistSN)+1)
	IF LEN(@IdentifyTempSN)<@IdentifyTempSN
	SET @IdentifyTempSN= REPLACE(right(cast(power(10,9) as varchar)+@IdentifyTempSN,@SNLength),'0',@PlaceHolder)
	SET @returnChar=@BillNo + @IdentifyTempSN
	END
	ELSE IF @IdentifyExistNo=''
	SET @returnChar=@BillNo+ REPLACE(right(cast(power(10,9) as varchar)+@SerialNumber,@SNLength),'0',@PlaceHolder)
	ELSE IF @IdentifyExistNo IS NULL
	SET @returnChar=@BillNo+ REPLACE(right(cast(power(10,9) as varchar)+@SerialNumber,@SNLength),'0',@PlaceHolder)
END
GO
/****** Object:  Default [DF__Desktop_P__IsDel__6477ECF3]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Desktop_Permission] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Desktop_P__IsDra__656C112C]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Desktop_Permission] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Desktop_I__IsDel__5EBF139D]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Desktop_ItemTab] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Desktop_I__IsDra__5FB337D6]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Desktop_ItemTab] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Desktop_I__IsDel__59063A47]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Desktop_Item] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Desktop_I__IsDra__59FA5E80]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Desktop_Item] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Desktop_G__IsDel__534D60F1]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Desktop_GridField] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Desktop_G__IsDra__5441852A]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Desktop_GridField] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_WorkT__IsDel__4222D4EF]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkToDoListHistory] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_WorkT__IsDra__4316F928]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkToDoListHistory] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_WorkT__IsDel__3C69FB99]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkToDoList] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_WorkT__IsDra__3D5E1FD2]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkToDoList] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_WorkN__IsDel__4D94879B]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkNodeInstanceHistory] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_WorkN__IsDra__4E88ABD4]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkNodeInstanceHistory] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_WorkN__IsDel__36B12243]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkNodeInstance] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_WorkN__IsDra__37A5467C]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkNodeInstance] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_WorkN__IsDel__30F848ED]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkNode] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_WorkN__IsDra__31EC6D26]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkNode] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_WorkL__IsDel__2B3F6F97]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkLine] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_WorkL__IsDra__2C3393D0]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkLine] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_WorkF__IsDel__47DBAE45]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkFlowInstanceHistory] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_WorkF__IsDra__48CFD27E]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkFlowInstanceHistory] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_WorkF__IsDel__25869641]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkFlowInstance] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_WorkF__IsDra__267ABA7A]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkFlowInstance] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_WorkF__IsDel__1FCDBCEB]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkFlow] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_WorkF__IsDra__20C1E124]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_WorkFlow] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_SubWo__IsDel__1A14E395]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_SubWorkFlow] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_SubWo__IsDra__1B0907CE]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_SubWorkFlow] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_NodeB__IsDel__145C0A3F]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_NodeBtnConfig] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_NodeB__IsDra__15502E78]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_NodeBtnConfig] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_FlowP__IsDel__0EA330E9]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_FlowProxy] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_FlowP__IsDra__0F975522]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_FlowProxy] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_FlowC__IsDel__08EA5793]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_FlowClass] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_FlowC__IsDra__09DE7BCC]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_FlowClass] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Bpm_FlowB__IsDel__03317E3D]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_FlowBtn] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Bpm_FlowB__IsDra__0425A276]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Bpm_FlowBtn] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_UserR__IsDel__4F12BBB9]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_UserRole] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_UserR__IsDra__5006DFF2]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_UserRole] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_UserQ__IsDel__4959E263]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_UserQuckMenu] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_UserQ__IsDra__4A4E069C]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_UserQuckMenu] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_UserP__IsDel__603D47BB]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_UserPermissionFun] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_UserP__IsDra__61316BF4]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_UserPermissionFun] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_UserP__IsDel__5A846E65]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_UserPermissionField] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_UserP__IsDra__5B78929E]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_UserPermissionField] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_UserP__IsDel__54CB950F]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_UserPermissionData] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_UserP__IsDra__55BFB948]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_UserPermissionData] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_UserG__IsDel__793DFFAF]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_UserGrid] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_UserG__IsDra__7A3223E8]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_UserGrid] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_User__IsDele__43A1090D]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_User] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_User__IsDraf__44952D46]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_User] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_TempU__IsDel__3DE82FB7]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_TempUser] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_TempU__IsDra__3EDC53F0]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_TempUser] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Syste__IsDel__73852659]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_SystemSet] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Syste__IsDra__74794A92]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_SystemSet] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_RoleF__IsDel__681373AD]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_RoleForm] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_RoleF__IsDra__690797E6]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_RoleForm] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Role__IsDele__382F5661]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Role] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Role__IsDraf__39237A9A]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Role] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Permi__IsDel__625A9A57]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_PermissionFun] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Permi__IsDra__634EBE90]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_PermissionFun] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Permi__IsDel__5CA1C101]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_PermissionField] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Permi__IsDra__5D95E53A]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_PermissionField] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Permi__IsDel__56E8E7AB]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_PermissionData] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Permi__IsDra__57DD0BE4]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_PermissionData] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_PageC__IsDel__65F62111]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_PageCache] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_PageC__IsDra__66EA454A]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_PageCache] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Organ__IsDel__51300E55]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Organization] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Organ__IsDra__5224328E]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Organization] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Modul__IsDel__32767D0B]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Module] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Modul__IsDra__336AA144]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Module] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Menu__IsDele__2CBDA3B5]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Menu__IsDraf__2DB1C7EE]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Menu] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_IconM__IsDel__6DCC4D03]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_IconManage] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_IconM__IsDra__6EC0713C]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_IconManage] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_GridF__IsDel__2704CA5F]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_GridField] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_GridF__IsDra__27F8EE98]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_GridField] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_GridB__IsDel__214BF109]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_GridButton] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_GridB__IsDra__22401542]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_GridButton] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Grid__IsDele__1B9317B3]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Grid] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Grid__IsDraf__1C873BEC]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Grid] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_FormF__IsDel__15DA3E5D]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_FormField] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_FormF__IsDra__16CE6296]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_FormField] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Form__IsDele__10216507]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Form] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Form__IsDraf__11158940]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Form] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Field__IsDel__0A688BB1]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Field] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Field__IsDra__0B5CAFEA]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Field] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Dicti__IsDel__04AFB25B]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Dictionary] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Dicti__IsDra__05A3D694]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Dictionary] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_BindD__IsDel__4B7734FF]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_BindDictionary] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_BindD__IsDra__4C6B5938]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_BindDictionary] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_BillC__IsDel__45BE5BA9]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_BillCodeRule] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_BillC__IsDra__46B27FE2]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_BillCodeRule] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Attac__IsDel__40058253]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_AttachModuleBind] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Attac__IsDra__40F9A68C]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_AttachModuleBind] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Sys_Attac__IsDel__7EF6D905]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Attachment] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Sys_Attac__IsDra__7FEAFD3E]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Sys_Attachment] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__OrgM_EmpD__IsDel__3A4CA8FD]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[OrgM_EmpDeptDuty] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__OrgM_EmpD__IsDra__3B40CD36]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[OrgM_EmpDeptDuty] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__OrgM_Emp__IsDele__3493CFA7]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[OrgM_Emp] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__OrgM_Emp__IsDraf__3587F3E0]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[OrgM_Emp] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__OrgM_Duty__IsDel__2EDAF651]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[OrgM_Duty] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__OrgM_Duty__IsDra__2FCF1A8A]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[OrgM_Duty] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__OrgM_Dept__IsDel__29221CFB]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[OrgM_DeptDuty] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__OrgM_Dept__IsDra__2A164134]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[OrgM_DeptDuty] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__OrgM_Dept__IsDel__236943A5]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[OrgM_Dept] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__OrgM_Dept__IsDra__245D67DE]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[OrgM_Dept] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Msg_To__IsDelete__01142BA1]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Msg_To] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Msg_To__IsDraft__02084FDA]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Msg_To] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Msg_Templ__IsDel__06CD04F7]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Msg_Template] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Msg_Templ__IsDra__07C12930]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Msg_Template] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Msg_SendL__IsDel__7B5B524B]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Msg_SendLog] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Msg_SendL__IsDra__7C4F7684]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Msg_SendLog] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Msg_Event__IsDel__6A30C649]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Msg_EventNotify] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Msg_Event__IsDra__6B24EA82]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Msg_EventNotify] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Msg_Cc__IsDelete__75A278F5]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Msg_Cc] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Msg_Cc__IsDraft__76969D2E]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Msg_Cc] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Msg_Bcc__IsDelet__6FE99F9F]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Msg_Bcc] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Msg_Bcc__IsDraft__70DDC3D8]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Msg_Bcc] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Monitor_O__IsDel__1DB06A4F]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Monitor_OpExecuteTime] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Monitor_O__IsDra__1EA48E88]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Monitor_OpExecuteTime] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Log_Opera__IsDel__17F790F9]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Log_Operate] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Log_Opera__IsDra__18EBB532]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Log_Operate] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Log_Login__IsDel__123EB7A3]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Log_Login] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Log_Login__IsDra__1332DBDC]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Log_Login] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
/****** Object:  Default [DF__Log_Excep__IsDel__0C85DE4D]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Log_Exception] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__Log_Excep__IsDra__0D7A0286]    Script Date: 06/22/2017 11:31:48 ******/
ALTER TABLE [dbo].[Log_Exception] ADD  DEFAULT ((0)) FOR [IsDraft]
GO
